package com.mo.motoast

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.core.*
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.BasicText
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.CheckCircle
import androidx.compose.material.icons.filled.Info
import androidx.compose.material.icons.filled.Warning
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class ToastSdK {

    companion object {
        @Composable
        fun showComposeToast(
            message: String,
            duration: Int = ToastDuration.Short,
            toastType: ToastType = ToastType.Default
        ) {
            val showToast by rememberUpdatedState(duration > 0)
            val coroutineScope = rememberCoroutineScope()

            DisposableEffect(showToast) {
                if (showToast) {
                    coroutineScope.launch {
                        delay(duration.toLong())
                        ToastState.showToast = false
                    }
                }
                onDispose { }
            }

            if (showToast) {
                Toast(toastType, message)
            }
        }
    }

}

@OptIn(ExperimentalAnimationApi::class)
@Composable
private fun Toast(toastType: ToastType, message: String) {
    val icon = when (toastType) {
        ToastType.Default -> Icons.Default.Info
        ToastType.Success -> Icons.Default.CheckCircle
        ToastType.Warning -> Icons.Default.Warning
    }

    val color = when (toastType) {
        ToastType.Default -> Color.Gray
        ToastType.Success -> Color.Green
        ToastType.Warning -> Color.Red
    }

    val transition = updateTransition(true)

    val offsetX by transition.animateDp(
        transitionSpec = {
            if (targetState) {
                keyframes {
                    durationMillis = 300
                    0.dp at 0 with LinearOutSlowInEasing
                    16.dp at 150 with FastOutLinearInEasing
                }
            } else {
                keyframes {
                    durationMillis = 300
                    16.dp at 0 with LinearOutSlowInEasing
                    0.dp at 150 with FastOutLinearInEasing
                }
            }
        }, label = ""
    ) { targetState -> if (targetState) 16.dp else 0.dp }

    Box(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp)
            .offset(x = offsetX)
            .background(color = color.copy(alpha = 0.9f))
            .padding(16.dp),
        contentAlignment = Alignment.Center
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.spacedBy(8.dp)
        ) {
            Icon(imageVector = icon, contentDescription = null, tint = Color.White)
            Text(text = message, color = Color.White)
        }
    }
}
enum class ToastType {
    Default,
    Success,
    Warning
}

object ToastState {
    var showToast by mutableStateOf(false)
}

object ToastDuration {
    const val Short = 2000
    const val Long = 3500
}

@Preview
@Composable
fun ToastPreview() {

        ToastSdK.showComposeToast(
            message = "This is a sample toast",
            duration = ToastDuration.Short,
            toastType = ToastType.Success
        )

}